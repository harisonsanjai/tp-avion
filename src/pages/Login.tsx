import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import FormulaireLogin from '../components/FormulaireLogin';
import './Login.css';
import React from 'react';

const Login: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Avion</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large"></IonTitle>
          </IonToolbar>
        </IonHeader>
        <FormulaireLogin />
      </IonContent>
    </IonPage>
  );
};

export default Login;