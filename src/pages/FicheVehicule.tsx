import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import FicheCard from '../components/FicheCard';
import './FicheVehicule.css';
import React from 'react';

const FicheVehicule: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Avion</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large"></IonTitle>
          </IonToolbar>
        </IonHeader>
        <FicheCard />
      </IonContent>
    </IonPage>
  );
};

export default FicheVehicule;