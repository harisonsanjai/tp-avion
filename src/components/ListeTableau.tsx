import './ListeTableau.css';
import { IonItem, IonLabel,IonCard,IonInput} from '@ionic/react';
import React from 'react';
import axios from 'axios';

interface ContainerProps { }

const ListeTableau: React.FC<ContainerProps> = () => {
    
        axios.get("https://tp-avion-production.up.railway.app/avions").then((res)=>{

            const box = document.getElementById('list');
            var blabla= res.data ;
            var val="<div id='list'><table>";
            for(var i=0;i<blabla['data'].length;i++)
            {
                val=val+" <tr><td><a href='/fiche?id="+blabla['data'][i]['id']+"'><td><ion-img src='src/assets/img/'<"+blabla['data'][i]['photo']+"></ion-img></a></td>";
                val=val+" <td>"+blabla['data'][i]['nom']+"</td></tr>";
            }
            val=val+"</table></div>";
             if (box != undefined) {
                box.innerHTML = val;
              }

        }).catch((error)=>{
          alert(error);
        })
        
  return (
    
    <IonCard>
        <h1>Liste des Avions</h1>
            <div id="list">
        </div>
    </IonCard>
  );

};

export default ListeTableau;