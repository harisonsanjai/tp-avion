import './FormulaireLogin.css';
import { IonItem, IonLabel,IonCard,IonInput} from '@ionic/react';
import { useState } from 'react';
import axios from 'axios';
import React from 'react';

interface ContainerProps { }

const FormulaireLogin: React.FC<ContainerProps> = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
    const login = () => {
      axios.post("https://tp-avion-production.up.railway.app/Utilisateur?email=m@gmail.com&Pwd=ok").then((res)=>{
          window.location.href="/liste";
      }).catch((error)=>{
        alert(error);
      })
    };
  return (

    <IonCard>
        <h1>Se connecter</h1>
        <IonLabel>Nom : </IonLabel>

        <IonInput type="text" value={"m@gmail.com"} name="email" placeholder="Entrez votre email" onIonChange={e => setUsername(e.detail.value!)} ></IonInput>

        <IonLabel>Mot de passe : </IonLabel>

        <IonInput type="password" value={"ok"} name="mdp" placeholder="Entrez votre mot de passe" onIonChange={e => setPassword(e.detail.value!)}></IonInput>


        <IonItem button onClick={() => login()}>
            <IonLabel>Login</IonLabel>
        </IonItem>
    </IonCard>
  );

};

export default FormulaireLogin;