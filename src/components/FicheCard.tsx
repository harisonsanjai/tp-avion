import './FicheCard.css';
import { IonItem, IonLabel,IonCard,IonInput} from '@ionic/react';
import React from 'react';
import axios from 'axios';

interface ContainerProps { }

const FicheCard: React.FC<ContainerProps> = () => {

  const queryString = window.location.search;
  const urlParams = new URLSearchParams(queryString);
  const id = urlParams.get('id');

  axios.get("https://tp-avion-production.up.railway.app/fiche?id="+id+"").then((res)=>
  {
    const box = document.getElementById('list');
    var blabla= res.data ;
    var val="<div id='list'><table><tr><td>Photo</td>  <td>Nom de l'avion</td> <td>Date expiration Assurance</td><td>Date entretien</td><td>Intitule Entretien</td> <td>Montant entretien</td><td>Kilometrage</td></tr>";
    for(var i=0;i<blabla['data'].length;i++)
    {

        val=val+" <tr><td><ion-img src='src/assets/img/'<"+blabla['data'][i]['photo']+"></ion-img></td>";
        val=val+" <td>"+blabla['data'][i]['nom']+"</td>";
        val=val+" <td>"+blabla['data'][i]['date_expiration_assurance ']+"</td>";
        val=val+" <td>"+blabla['data'][i]['date_entretien']+"</td>";
        val=val+" <td>"+blabla['data'][i]['intitule']+"</td>";
        val=val+" <td>"+blabla['data'][i]['montant']+"</td>";
        val=val+" <td>"+blabla['data'][i]['kilometrage']+"</td></tr>";
    }
    val=val+"</table></div>";
     if (box != undefined) {
        box.innerHTML = val;
      }

}).catch((error)=>{
  alert(error);
})
  
  return (
    <IonCard>
        <h1>Fiche de l'avion</h1>
        <div id="list"></div>
    </IonCard>
  );
};

export default FicheCard;
