create database avion;
\c avion;

CREATE TABLE Assurance (id SERIAL NOT NULL, idAvion SERIAL NOT NULL, date_paiment date, date_expiration date, montant Double precision);
CREATE TABLE Avion (id SERIAL NOT NULL, photo varchar(255), nom varchar(255),kilometrage double PRECISION, date_expiration_Assurance date,PRIMARY KEY (id));
CREATE TABLE entretien (id SERIAL NOT NULL,idAvion SERIAL NOT NULL, date_entretien date, intitule varchar(255), montant Double precision);
CREATE TABLE Utilisateur (id SERIAL NOT NULL, nom varchar(255), email varchar(255), pwd varchar(255), PRIMARY KEY (id));
ALTER TABLE entretien ADD CONSTRAINT FKentretien875782 FOREIGN KEY (idAvion) REFERENCES Avion (id);
ALTER TABLE Assurance ADD CONSTRAINT FKAssurance14862 FOREIGN KEY (idAvion) REFERENCES Avion (id);

-- INSERT INTO Assurance(id,idAvion, date_paiment, date_expiration, montant) VALUES (?, ?, ?, ?);
-- INSERT INTO Avion(id, photo, nom) VALUES (?, ?, ?);
-- INSERT INTO entretien(id, date_entretien, intitule, montant) VALUES (?, ?, ?, ?);
-- INSERT INTO Utilisateur(id, nom, email, pwd) VALUES (?, ?, ?, ?);

INSERT INTO Avion( photo, nom) VALUES ('image.jpg', 'avvsss');
INSERT INTO Avion( photo, nom) VALUES ('image2.jpg', 'forceone');
INSERT INTO Avion( photo, nom,kilometrage,date_expiration_Assurance) VALUES ('image2.jpg', 'forceone',30000,2022-12-30);

UPDATE Avion set kilometrage=20000 where id=5;
UPDATE Avion set date_expiration_Assurance='2022-12-22' where id=5;

INSERT INTO Assurance(idAvion, date_paiment, date_expiration, montant) VALUES (1,'22-12-2022' ,'22-12-2022' , 2222);
INSERT INTO Assurance(idAvion, date_paiment, date_expiration, montant) VALUES (1,'23-12-2022' ,'01-01-2023' , 2222);
INSERT INTO entretien( date_entretien, intitule, montant) VALUES ( '22-12-2022', 'vidange', 50000);

INSERT INTO Utilisateur( nom, email, pwd) VALUES ('m', 'm@gmail.com', 'ok');
-- ALTER TABLE entretien DROP CONSTRAINT FKentretien875782;
-- ALTER TABLE Assurance DROP CONSTRAINT FKAssurance14862;
-- DROP TABLE IF EXISTS Assurance CASCADE;
-- DROP TABLE IF EXISTS Avion CASCADE;
-- DROP TABLE IF EXISTS entretien CASCADE;
-- DROP TABLE IF EXISTS User CASCADE;